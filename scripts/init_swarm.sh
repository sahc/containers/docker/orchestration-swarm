#!/usr/bin/env bash
# Copyright Axity
# Author: Sergio Hume
# Email: sergio.hume@axity.com
# Date: 16 Feb 2018
#

function echoe(){
    echo "************************************************************************************************"
    echo "      "$1
    echo "************************************************************************************************"
    sleep 3
}

function initilize_swarm(){
    echoe "  Initializing Docker Swarm Cluster"
    docker swarm leave --force >/dev/null 2>&1
    docker swarm init

    echoe "  Create new overlay network for service communication [proxy]"
    docker network rm proxy >/dev/null 2>&1
    docker network create -d overlay proxy
}
